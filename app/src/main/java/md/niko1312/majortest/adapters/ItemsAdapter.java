package md.niko1312.majortest.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import md.niko1312.majortest.R;
import md.niko1312.majortest.data.models.ItemModel;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {
    private ArrayList<ItemModel> items;
    private OnListItemClickListener onListItemClickListener;

    public ItemsAdapter() {
        items = new ArrayList<>();
    }

    public void setItems(@NonNull ArrayList<ItemModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setOnListItemClickListener(OnListItemClickListener onListItemClickListener) {
        this.onListItemClickListener = onListItemClickListener;
    }

    @NonNull
    @Override
    public ItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ItemsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(items.get(position), onListItemClickListener);
    }

    @Override
    public int getItemCount() {
        return items.size() > 0 ? items.size() : 0;
    }

    private TextView tvTitle;

    class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.item_title);
        }

        void bind(ItemModel itemModel, OnListItemClickListener onListItemClickListener) {
            tvTitle.setText(itemModel.title());
            itemView.setOnClickListener(v -> {
                if (onListItemClickListener != null) {
                    onListItemClickListener.onItemClick(items, getAdapterPosition());
                }
            });
        }
    }

    public interface OnListItemClickListener {
        void onItemClick(ArrayList<ItemModel> items, int position);
    }
}
