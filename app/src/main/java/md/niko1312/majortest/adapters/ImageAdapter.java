package md.niko1312.majortest.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import md.niko1312.majortest.R;
import md.niko1312.majortest.data.models.ItemModel;
import md.niko1312.majortest.media.ImageLoader;

public class ImageAdapter extends PagerAdapter {
    private final ImageLoader imageLoader;
    private ArrayList<ItemModel> items;

    public ImageAdapter(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    @Override
    public int getCount() {
        return items.size() > 0 ? items.size() : 0;
    }

    public void setItems(ArrayList<ItemModel> items) {
        this.items = items;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (layoutInflater != null) {
            View itemView = layoutInflater.inflate(R.layout.pager_item, container, false);
            ItemModel item = items.get(position);
            ImageView imageView = itemView.findViewById(R.id.item_image);
            imageLoader.loadImage(item.img(), imageView, message -> {
                Toast.makeText(container.getContext(), message, Toast.LENGTH_SHORT).show();
            });
            TextView title = itemView.findViewById(R.id.title);
            title.setText(item.title());
            container.addView(itemView);

            return itemView;
        } else {
            return super.instantiateItem(container, position);
        }
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
