package md.niko1312.majortest.ui;

import android.support.v4.app.Fragment;

public interface PagerProvider {
    Fragment getInstance();
}
