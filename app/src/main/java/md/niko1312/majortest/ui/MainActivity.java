package md.niko1312.majortest.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import java.util.ArrayList;

import md.niko1312.majortest.R;
import md.niko1312.majortest.data.models.ItemModel;
import md.niko1312.majortest.ui.fragments.BasePresenter;
import md.niko1312.majortest.ui.fragments.DetailsFragment;
import md.niko1312.majortest.ui.fragments.ListFragment;

public class MainActivity extends AppCompatActivity {
    public static final String ITEM_POSITION = "position";
    public static final String ITEMS = "items";

    private BasePresenter basePresenter;
    private FrameLayout fragmentContainer;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Major_Base);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentContainer = findViewById(R.id.fragment_container);

        MainComponent mainComponent = MainInjector.obtain(this);
        basePresenter = mainComponent.basePresenter();

        showList();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

    private void showList() {
        Fragment listFragment = new ListFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, listFragment)
                .commitAllowingStateLoss();
        onList();
    }

    public void showDetails(ArrayList<ItemModel> items, int position) {
        Fragment listFragment = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ITEM_POSITION, position);
        bundle.putParcelableArrayList(ITEMS, items);
        listFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, listFragment)
                .commitAllowingStateLoss();
        onDetails();
    }

    public void onList() {
        toolbar.setTitle("List");
        toolbar.setNavigationIcon(null);
    }

    public void onDetails() {
        toolbar.setTitle("Details");
        toolbar.setNavigationIcon(getDrawable(R.drawable.ic_arrow_back_24dp));
        toolbar.setNavigationOnClickListener(v -> {
            showList();
        });
    }
}
