package md.niko1312.majortest.ui;

import dagger.Component;
import md.niko1312.majortest.adapters.ImageAdapter;
import md.niko1312.majortest.adapters.ItemsAdapter;
import md.niko1312.majortest.injection.ApiModule;
import md.niko1312.majortest.injection.ApplicationComponent;
import md.niko1312.majortest.media.ImageLoader;
import md.niko1312.majortest.ui.fragments.BasePresenter;
import md.niko1312.majortest.ui.fragments.MainModule;

@Component(modules = {ApiModule.class, MainModule.class},
        dependencies = ApplicationComponent.class)
public interface MainComponent {
    BasePresenter basePresenter();

    ImageLoader imageLoader();

    ImageAdapter imageAdapter();

    ItemsAdapter itemsAdapter();
}
