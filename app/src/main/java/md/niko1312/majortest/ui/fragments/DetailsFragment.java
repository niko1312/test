package md.niko1312.majortest.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.ArrayList;

import md.niko1312.majortest.R;
import md.niko1312.majortest.data.models.ItemModel;
import md.niko1312.majortest.ui.MainActivity;
import md.niko1312.majortest.ui.PagerProvider;

public class DetailsFragment extends BaseFragment implements PagerProvider {
    @Override
    public int layout() {
        return R.layout.fragment_details;
    }

    @Override
    public Fragment getInstance() {
        return this;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null) {
            int position = args.getInt(MainActivity.ITEM_POSITION);
            ArrayList<ItemModel> items = args.getParcelableArrayList(MainActivity.ITEMS);
            ViewPager viewPager = view.findViewById(R.id.pager_container);
            imageAdapter.setItems(items);
            viewPager.setAdapter(imageAdapter);
            viewPager.setCurrentItem(position);
        }
    }
}
