package md.niko1312.majortest.ui.fragments;

import java.util.ArrayList;

import io.reactivex.Single;
import md.niko1312.majortest.data.api.MajorApi;
import md.niko1312.majortest.data.models.ItemModel;

public class BaseInteractor {
    private final MajorApi majorApi;

    BaseInteractor(MajorApi majorApi) {
        this.majorApi = majorApi;
    }

    public Single<ArrayList<ItemModel>> updateList() {
        return majorApi.getItems();
    }
}
