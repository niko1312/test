package md.niko1312.majortest.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import md.niko1312.majortest.R;
import md.niko1312.majortest.data.models.ItemModel;
import md.niko1312.majortest.ui.MainActivity;
import md.niko1312.majortest.ui.PagerProvider;

import static android.widget.LinearLayout.VERTICAL;

public class ListFragment extends BaseFragment implements PagerProvider, BasePresenter.View {
    private RelativeLayout loadingView;
    private TextView emptyText;

    @Override
    public int layout() {
        return R.layout.fragment_list;
    }

    @Override
    public Fragment getInstance() {
        return this;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadingView = view.findViewById(R.id.loading_view);

        basePresenter.attachView(this);
        basePresenter.updateList();

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(fabView -> basePresenter.updateList());

        emptyText = view.findViewById(R.id.empty_text);
        RecyclerView listView = view.findViewById(R.id.items_list);
        listView.setAdapter(itemsAdapter);
        listView.setLayoutManager(new LinearLayoutManager(getActivity(), VERTICAL, false));

        DividerItemDecoration itemDecorator =
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.rv_divider));
        listView.addItemDecoration(itemDecorator);
        itemsAdapter.setOnListItemClickListener((items, position) -> {
            ((MainActivity) getActivity()).showDetails(items, position);
        });
    }

    @Override
    public void displayItems(ArrayList<ItemModel> itemModelList) {
        emptyText.setVisibility(itemModelList.size() > 0 ? View.GONE : View.VISIBLE);
        itemsAdapter.setItems(itemModelList);
    }

    @Override
    public void showLoadingView() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            loadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoadingView() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            loadingView.setVisibility(View.GONE);
        }
    }
}
