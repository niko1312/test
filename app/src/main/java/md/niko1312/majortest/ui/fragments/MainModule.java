package md.niko1312.majortest.ui.fragments;

import dagger.Module;
import dagger.Provides;
import md.niko1312.majortest.adapters.ImageAdapter;
import md.niko1312.majortest.adapters.ItemsAdapter;
import md.niko1312.majortest.data.api.MajorApi;
import md.niko1312.majortest.media.ImageLoader;

@Module
public class MainModule {
    @Provides
    BasePresenter basePresenter(BaseInteractor baseInteractor) {
        return new BasePresenter(baseInteractor);
    }

    @Provides
    BaseInteractor baseInteractor(MajorApi majorApi) {
        return new BaseInteractor(majorApi);
    }

    @Provides
    ImageLoader imageLoader() {
        return new ImageLoader();
    }

    @Provides
    ImageAdapter imageAdapter(ImageLoader imageLoader) {
        return new ImageAdapter(imageLoader);
    }

    @Provides
    ItemsAdapter itemsAdapter() {
        return new ItemsAdapter();
    }
}
