package md.niko1312.majortest.ui;

import android.content.Context;
import android.support.annotation.NonNull;

import md.niko1312.majortest.injection.ApplicationInjector;
import md.niko1312.majortest.utils.InstantiationForbiddenError;

import static dagger.internal.Preconditions.checkNotNull;

public final class MainInjector {
    private MainInjector() {
        throw new InstantiationForbiddenError();
    }

    public static MainComponent obtain(@NonNull Context context) {
        checkNotNull(context);
        return DaggerMainComponent.builder()
                .applicationComponent(ApplicationInjector.obtain(context))
                .build();
    }
}
