package md.niko1312.majortest.ui.fragments;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import md.niko1312.majortest.data.models.ItemModel;

import static dagger.internal.Preconditions.checkNotNull;

public class BasePresenter {
    private final BaseInteractor baseInteractor;
    private CompositeDisposable compositeDisposable;
    private View view;

    BasePresenter(BaseInteractor baseInteractor) {
        this.baseInteractor = baseInteractor;
    }

    public void attachView(@NonNull View view) {
        this.view = checkNotNull(view);
    }

    public void updateList() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }

        compositeDisposable.add(baseInteractor.updateList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> view.showLoadingView())
                .subscribe(items -> {
                    view.hideLoadingView();
                    view.displayItems(items);
                }, throwable -> {
                    view.hideLoadingView();
                }));
    }

    public interface View {
        void displayItems(ArrayList<ItemModel> itemModelList);

        void showLoadingView();

        void hideLoadingView();
    }
}
