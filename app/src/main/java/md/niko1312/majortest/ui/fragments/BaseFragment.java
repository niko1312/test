package md.niko1312.majortest.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import md.niko1312.majortest.adapters.ImageAdapter;
import md.niko1312.majortest.adapters.ItemsAdapter;
import md.niko1312.majortest.media.ImageLoader;
import md.niko1312.majortest.ui.MainComponent;
import md.niko1312.majortest.ui.MainInjector;
import md.niko1312.majortest.ui.PagerProvider;

public abstract class BaseFragment extends Fragment implements PagerProvider {
    protected View mView;
    protected BasePresenter basePresenter;
    protected ImageLoader imageLoader;
    protected ItemsAdapter itemsAdapter;
    protected ImageAdapter imageAdapter;

    public abstract int layout();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainComponent mainComponent = MainInjector.obtain(getActivity());
        basePresenter = mainComponent.basePresenter();
        imageLoader = mainComponent.imageLoader();
        imageAdapter = mainComponent.imageAdapter();
        itemsAdapter = mainComponent.itemsAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(layout(), container, false);
        return mView;
    }
}
