package md.niko1312.majortest.media;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import md.niko1312.majortest.R;

public final class ImageLoader {
    @DrawableRes
    private int placeHolder = R.drawable.ic_empty_image_24dp;

    public void loadImage(@Nullable String url, @NonNull ImageView imageView, @NonNull ImageLoaderListener imageLoaderListener) {
        GlideApp.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.ic_empty_image_24dp)
                .fitCenter()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        imageLoaderListener.onLoadError(e.getMessage());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }

    public interface ImageLoaderListener {
        void onLoadError(String message);
    }
}
