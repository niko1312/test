package md.niko1312.majortest;

import android.app.Application;
import android.support.annotation.MainThread;

import md.niko1312.majortest.injection.ApplicationComponent;

public class MajorApp extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent();
    }

    @MainThread
    public ApplicationComponent applicationComponent() {
        if (applicationComponent == null) {
            applicationComponent = ApplicationComponent.Factory.create(this);
        }

        return applicationComponent;
    }
}
