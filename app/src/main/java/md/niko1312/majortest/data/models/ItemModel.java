package md.niko1312.majortest.data.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import static dagger.internal.Preconditions.checkNotNull;

@AutoValue
public abstract class ItemModel implements Parcelable {
    public abstract int id();

    @NonNull
    public abstract String title();

    @NonNull
    public abstract String img();

    @NonNull
    public static TypeAdapter<ItemModel> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_ItemModel.GsonTypeAdapter(checkNotNull((gson)));
    }
}
