package md.niko1312.majortest.data.api;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import io.reactivex.Single;
import md.niko1312.majortest.data.models.ItemModel;
import retrofit2.http.GET;

public interface MajorApi {
    @NonNull
    @GET("/androids")
    Single<ArrayList<ItemModel>> getItems();
}
