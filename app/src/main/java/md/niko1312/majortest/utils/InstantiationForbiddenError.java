package md.niko1312.majortest.utils;

public final class InstantiationForbiddenError extends AssertionError {
  private static final String MESSAGE = "No instances allowed";

  public InstantiationForbiddenError() {
    super(MESSAGE);
  }
}
