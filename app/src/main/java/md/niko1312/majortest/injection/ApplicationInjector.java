package md.niko1312.majortest.injection;

import android.content.Context;
import android.support.annotation.NonNull;

import md.niko1312.majortest.MajorApp;
import md.niko1312.majortest.utils.InstantiationForbiddenError;

import static dagger.internal.Preconditions.checkNotNull;

public final class ApplicationInjector {
    private ApplicationInjector() {
        throw new InstantiationForbiddenError();
    }

    @NonNull
    public static ApplicationComponent obtain(@NonNull Context context) {
        return ((MajorApp) checkNotNull(
                context).getApplicationContext()).applicationComponent();
    }
}
