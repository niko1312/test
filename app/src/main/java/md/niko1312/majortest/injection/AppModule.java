package md.niko1312.majortest.injection;

import android.app.Application;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    @NonNull
    private final Application application;

    AppModule(@NonNull Application application) {
        this.application = application;
    }

    @Provides
    Application applicationContext() {
        return application;
    }
}
