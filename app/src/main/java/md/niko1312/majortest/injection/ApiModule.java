package md.niko1312.majortest.injection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import dagger.Module;
import dagger.Provides;
import md.niko1312.majortest.BuildConfig;
import md.niko1312.majortest.data.api.MajorApi;
import md.niko1312.majortest.data.api.MajorTypeAdapterFactory;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {
    @Provides
    MajorApi getMajorApi(Gson gson, OkHttpClient client) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .baseUrl(BuildConfig.MAJOR_URL)
                .build()
                .create(MajorApi.class);
    }

    @Provides
    Gson getGson() {
        return new GsonBuilder().registerTypeAdapterFactory(MajorTypeAdapterFactory.create()).create();
    }

    @Provides
    static OkHttpClient okHttpClient() {
        return createOkHttpClient();
    }

    private static OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();

        return okBuilder.build();
    }
}
