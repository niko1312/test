package md.niko1312.majortest.injection;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
public final class ActivityContextModule {
    @NonNull
    private final AppCompatActivity activity;

    public ActivityContextModule(@NonNull AppCompatActivity activity) {
        this.activity = checkNotNull(activity);
    }

    @Provides
    AppCompatActivity appCompatActivityContext() {
        return activity;
    }

}
