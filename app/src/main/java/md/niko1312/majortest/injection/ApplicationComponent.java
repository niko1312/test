package md.niko1312.majortest.injection;

import android.app.Application;
import android.support.annotation.NonNull;

import dagger.Component;
import md.niko1312.majortest.utils.InstantiationForbiddenError;

import static dagger.internal.Preconditions.checkNotNull;

@Component(modules = {AppModule.class})
public interface ApplicationComponent {
    Application application();

    final class Factory {
        private Factory() {
            throw new InstantiationForbiddenError();
        }

        public static ApplicationComponent create(@NonNull Application application) {
            return DaggerApplicationComponent.builder()
                    .appModule(new AppModule(checkNotNull(application)))
                    .build();
        }
    }
}
